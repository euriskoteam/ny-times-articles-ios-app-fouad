//
//  NewsViewTests.swift
//  NYTimesTests
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import XCTest
@testable import NYTimes

class NewsViewTests: XCTestCase {

    var controllerUnderTest: NewsView?
    var sessionUnderTest: URLSession!
    
    override func setUp() {
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        controllerUnderTest = coreapp.mainStoryboard.instantiateViewController(withIdentifier: ViewIdentifier.NewsView) as? NewsView ?? NewsView()
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
    }

    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        controllerUnderTest = nil
        sessionUnderTest = nil
        super.tearDown()
    }

    //MARK:- My Test Functions
    func testNewsCoreData() {
        controllerUnderTest?.getNewsData(performFetch: true)
    }
    
    func testNewsSimpleApiCall() {
        
        // given
        let url = URL(string: "http://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/30.json?api-key=Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5")
        
        // 1
        let promise = expectation(description: "Completion handler invoked")
        var statusCode: Int?
        var responseError: Error?
        
        // when
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            // 2
            promise.fulfill()
        }
        dataTask.resume()
        // 3
        waitForExpectations(timeout: 5, handler: nil)
        
        // then
        XCTAssertNil(responseError) //We want the response error to be nil, else, the test should fail
        XCTAssertEqual(statusCode, 200) //We want the response status code to be 200, else, the test should fail
    }

}
