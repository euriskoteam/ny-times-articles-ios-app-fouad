//
//  Utilities.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit

class Utilities: NSObject {
    
    //MARK:- Parse Codable
    static func parseCodable <T : Codable> (data:Data?, type: T.Type, successHandler:@escaping (_ details: T) -> Void, errorHandler:@escaping (_ error:String) -> Void)  {
        
        guard let responseData = data, let decodedJson = try? JSONDecoder().decode(T.self, from: responseData) else {
            errorHandler(translate(key: "Parsing error, please contact support"))
            return
        }
        successHandler(decodedJson)
    }
    
    //MARK:- NSMutableString Builder From 2 Strings
    static func buildAttrStringFrom2Strings (string1Text:String, string1Color:UIColor, string1Font:UIFont, string1IsUnderLine:Bool = false, string2Text:String, string2Color:UIColor, string2Font:UIFont, string2IsUnderLine:Bool = false) -> NSMutableAttributedString {
        
        let final = NSMutableAttributedString()
        do {
            let tStr = NSMutableAttributedString.init(string: string1Text)
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: string1Font,
                NSAttributedString.Key.foregroundColor: string1Color
            ]
            if string1IsUnderLine {
                let underLineAttributes: [NSAttributedString.Key : Any] = [
                    NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
                ]
                tStr.addAttributes(underLineAttributes, range: NSMakeRange(0, tStr.length))
            }
            tStr.addAttributes(attributes, range: NSMakeRange(0, tStr.length))
            final.append(tStr)
        }
        do {
            let tStr = NSMutableAttributedString.init(string: string2Text)
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: string2Font,
                NSAttributedString.Key.foregroundColor: string2Color
            ]
            if string2IsUnderLine {
                let underLineAttributes: [NSAttributedString.Key : Any] = [
                    NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
                ]
                tStr.addAttributes(underLineAttributes, range: NSMakeRange(0, tStr.length))
            }
            tStr.addAttributes(attributes, range: NSMakeRange(0, tStr.length))
            final.append(tStr)
        }
        
        return final
    }
    
}
