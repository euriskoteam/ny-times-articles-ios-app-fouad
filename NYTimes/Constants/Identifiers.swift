//
//  Identifiers.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit

struct StoryboardIdentifier {
    static let Main: String = "Main"
}

struct ViewIdentifier {
    static let NewsView: String = "NewsView"
    static let NewsDetails: String = "NewsDetails"
}

struct CellIdentifier {
    static let NewsTableCell: String = "NewsTableCell"
}
