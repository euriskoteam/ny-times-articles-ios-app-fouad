//
//  Enums.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit

enum SegueType {
    case push
    case present
}

enum Animation {
    case fadeIn
    case fadeOut
}

enum Side {
    case top
    case bottom
    case right
    case left
}

enum JPEGQuality: CGFloat {
    case lowest = 0.0
    case low = 0.25
    case medium = 0.5
    case high = 0.75
    case highest = 1.0
}

enum Keys: String {
    case accessToken = "Access-Token"
    case appVersion = ""
}

enum Period: String {
    case sevenDays = "/7.json"
    case oneDay = "/1.json"
    case thirtyDays = "/30.json"
}

enum Section: String {
    case all = "/all-sections"
}
