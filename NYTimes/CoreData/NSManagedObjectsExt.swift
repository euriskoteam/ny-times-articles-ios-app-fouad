//
//  NSManagedObjectsExt.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import Foundation
import CoreData

extension NewsResults {
    
    class func createOrFetch(model: NewsModal.Result?) -> NewsResults? {
        
        guard let model = model, let moc = CoreDataStore.managedObjectContext else { return nil }
        let fetchRequest: NSFetchRequest<NewsResults> = NewsResults.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "publishedDate", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "id == \(model.id ?? 0)")
        
        var item: NewsResults?
        
        if let fetchedItem = try? moc.fetch(fetchRequest).first, fetchedItem != nil {
            item = fetchedItem
        } else {
            item = NewsResults(context: moc)
        }
        
        item?.abstract = model.abstract
        item?.adxKeywords = model.adxKeywords
        item?.assetID = Int64(model.assetID ?? 0)
        item?.byline = model.byline
        item?.column = model.column
        item?.id = Int64(model.id ?? 0)
        item?.publishedDate = model.publishedDate
        item?.section = model.section
        item?.source = model.source?.rawValue ?? ""
        item?.title = model.title
        item?.type = model.type?.rawValue ?? ""
        item?.url = model.url ?? ""
        item?.views = Int64(model.views ?? 0)
        let array = NewsMedia.createOrFetch(models: model.media, parentId: Int64(model.id ?? 0))
        item?.media? = NSSet(array: array)
        return item
    }
    
    class func createOrFetch(models: [NewsModal.Result]?) -> [NewsResults] {
        
        guard let models = models else { return [] }
        var items: [NewsResults] = []
        for model in models {
            guard let item = NewsResults.createOrFetch(model: model) else { continue }
            items.append(item)
        }
        return items
    }
    
}
extension NewsMedia {
    
    class func createOrFetch(model: NewsModal.Media?, parentId:Int64) -> NewsMedia? {
        
        guard let model = model, let moc = CoreDataStore.managedObjectContext else { return nil }
        let fetchRequest: NSFetchRequest<NewsMedia> = NewsMedia.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "newsResults.id == \(parentId)")
        
        var item: NewsMedia?
        
        if let fetchedItem = try? moc.fetch(fetchRequest).first, fetchedItem != nil {
            item = fetchedItem
        } else {
            item = NewsMedia(context: moc)
        }
        
        item?.approvedForSyndication = Int64(model.approvedForSyndication ?? 0)
        item?.caption = model.caption ?? ""
        item?.copyright = model.copyright ?? ""
        item?.subtype = model.subtype?.rawValue ?? ""
        item?.type = model.type?.rawValue ?? ""
        let array = NewsMediaMetadatum.createOrFetch(models: model.mediaMetadata, parentId: parentId)
        item?.mediaMetadata = NSSet(array: array)
        return item
    }
    
    class func createOrFetch(models: [NewsModal.Media]?, parentId:Int64) -> [NewsMedia] {
        
        guard let models = models else { return [] }
        var items: [NewsMedia] = []
        for model in models {
            guard let item = NewsMedia.createOrFetch(model: model, parentId: parentId) else { continue }
            items.append(item)
        }
        return items
    }
}
extension NewsMediaMetadatum {
    
    class func createOrFetch(model: NewsModal.MediaMetadatum?, parentId:Int64) -> NewsMediaMetadatum? {
        
        guard let model = model, let moc = CoreDataStore.managedObjectContext else { return nil }
        let fetchRequest: NSFetchRequest<NewsMediaMetadatum> = NewsMediaMetadatum.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "newsMedia.newsResults.id == \(parentId)")
        
        var item: NewsMediaMetadatum?
        
        if let fetchedItem = try? moc.fetch(fetchRequest).first, fetchedItem != nil {
            item = fetchedItem
        } else {
            item = NewsMediaMetadatum(context: moc)
        }
        
        item?.newsId = parentId
        item?.format = model.format?.rawValue ?? ""
        item?.height = Int32(model.height ?? 0)
        item?.width = Int32(model.width ?? 0)
        item?.url = model.url ?? ""
        
        return item
    }
    
    class func createOrFetch(models: [NewsModal.MediaMetadatum]?, parentId:Int64) -> [NewsMediaMetadatum] {
        
        guard let models = models else { return [] }
        var items: [NewsMediaMetadatum] = []
        for model in models {
            guard let item = NewsMediaMetadatum.createOrFetch(model: model, parentId: parentId) else { continue }
            items.append(item)
        }
        return items
    }
    
    class func fetchMetadataForNewsId(newsId:Int64) -> [NewsMediaMetadatum]? {
        guard let moc = CoreDataStore.managedObjectContext else { return nil }
        let fetchRequest: NSFetchRequest<NewsMediaMetadatum> = NewsMediaMetadatum.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "newsId == \(newsId)")
        let results = try? moc.fetch(fetchRequest)
        return results
        
    }
}
