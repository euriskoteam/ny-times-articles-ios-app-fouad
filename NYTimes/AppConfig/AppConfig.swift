//
//  AppConfig.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit
import Reachability
import Fabric
import Crashlytics

class AppConfig: NSObject {
    
    static let shared = AppConfig()
    
    private override init() {
        super.init()
        initDefaultValue()
    }
    
    //Global Vars Across The Entire App ..
    var _launchOptions = [UIApplication.LaunchOptionsKey: Any]()
    
    //Reachability
    let reachability = Reachability()!
    
    //Storyboards
    var mainStoryboard = UIStoryboard()
    
    //MARK:- Init The App Default Values
    func initDefaultValue() {
        
        //init Crashlytics
        Fabric.with([Crashlytics.self])
        
        //init Storyboards
        self.initStoryboards()
        
        //Init Reachability
        self.initReachabilityAndStartTheApp()
    }
    
    //MARK:- App Main Delegate Functions
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Save the launchOptions for later usage ..
        _launchOptions = launchOptions ?? [UIApplication.LaunchOptionsKey: Any]()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    func application( _ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

    }
    
    func application( _ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler:@escaping (UIBackgroundFetchResult) -> Void) {
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return true
    }
    
    //MARK:- init Storyboards
    func initStoryboards() {
        mainStoryboard = UIStoryboard.init(name: StoryboardIdentifier.Main, bundle: nil)
    }
    
    //MARK:- Init Reachability & Start the app
    func initReachabilityAndStartTheApp() {
        
        //declare this property where it won't go out of scope relative to your listener
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        self.startTheApp()
    }
    
    //MARK:- ------------------------ START THE APP ------------------------
    func startTheApp() {
        let mainView = self.mainStoryboard.instantiateViewController(withIdentifier: ViewIdentifier.NewsView) as? NewsView ?? NewsView()
        let mainNav = MainNavigation.init(rootViewController: mainView)
        
        appDelegate?.window?.rootViewController = mainNav
        appDelegate?.window?.makeKeyAndVisible()
        
    }
}
