//
//  APIManager.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import Foundation

//MARK:- API HTTP Parameters
typealias Parameters = [String: Any]

//MARK:- API HTTP Headers
typealias HTTPHeaders = [String: Any]

//MARK:- API CompletionHandler
typealias CompletionHandler = (_ response: ResponseData) -> Void

//MARK:- API Manager
struct APIManager {
    
    // MARK API Manager Shared Instance
    static let shared = APIManager.init()
    
    // MARK API Private Init
    private init() {
        //coreapi.timeoutInterval = 60
    }
    
    //MARK:- API SERVER REQUEST
    private func makeHttpRequest(endPoint: String, httpMethod: HTTPMethod, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, completionHandler: @escaping CompletionHandler) {
        var responseData = ResponseData()
        
        //Check Internet Connection
        if coreapp.reachability.connection == .none {
            responseData.status = ResponseStatus.conenctionTimeout.rawValue
            responseData.message = ResponseMessage.connectionTimeout
            completionHandler(responseData)
            return
        }
        
        //Check URL
        guard let url = URL(string: "\(coreapi.BaseUrl)\(endPoint)\(coreapi.ApiKey)") else {
            responseData.status = ResponseStatus.failure.rawValue
            responseData.message = ResponseMessage.serverUnreachable
            completionHandler(responseData)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        
        if let httpHeaders = headers as? [String : String] {
            urlRequest.allHTTPHeaderFields = httpHeaders
        }
        
        if let httpBody = parameters {
            do {
                urlRequest.httpBody = try NSKeyedArchiver.archivedData(withRootObject: httpBody, requiringSecureCoding: false)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        let task = coreapi.manager.dataTask(with: urlRequest) {
            (data, response, error) in
            
            DispatchQueue.main.async(execute: {
                
                guard error == nil else {
                    responseData.status = ResponseStatus.failure.rawValue
                    responseData.message = error?.localizedDescription ?? ResponseMessage.serverUnreachable
                    completionHandler(responseData)
                    return
                }
                
                guard let data = data else {
                    responseData.status = ResponseStatus.failure.rawValue
                    responseData.message = ResponseMessage.serverUnreachable
                    completionHandler(responseData)
                    return
                }
                
                responseData.data = data
                completionHandler(responseData)
            })
        }
        task.resume()
    }
}

//MARK:- API Manager Extension
extension APIManager {
    
    /******************** API Calls ********************/
    
    //MARK:- --------------------- 'GET' NEWS API CALL ---------------------
    func getNewsListForSection(section:Section, period:Period, completionHandler: @escaping CompletionHandler) {
        let endPoint = APIEndPoints.mostviewed + section.rawValue + period.rawValue
        self.makeHttpRequest(endPoint: endPoint, httpMethod: .get, parameters: nil, headers: nil) { (data) in
            completionHandler(data)
        }
    }
}
