//
//  APIEndPoints.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

//MARK:- API Endpoints
struct APIEndPoints {
    static let mostviewed = "/svc/mostpopular/v2/mostviewed"
}
