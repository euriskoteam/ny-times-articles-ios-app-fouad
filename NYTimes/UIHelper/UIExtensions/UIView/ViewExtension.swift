//
//  ViewExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit
import Foundation

extension UIView {
    func animate(withDuration duration: TimeInterval = 0.5, animation: Animation) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = animation == .fadeIn ? 1.0 : 0.0
        })
    }
    
    func loadAndSetupXib(fromNibNamed nibName: String) {
        if let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as? UIView {
            view.frame = self.bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(view)
        }
    }
    
    func customizeView(backgroundColor: UIColor, radius: CGFloat = 0) {
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = radius
    }
    
    func customizeBorder(color: UIColor, width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func addBorderWithColor(color: UIColor, width: CGFloat, from: Side) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        switch from {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
            break
        case .bottom:
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
            break
        case .right:
            border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
            break
        case .left:
                border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
            break
        }
        
        self.layer.addSublayer(border)
    }
    
    func isEnabled(enable: Bool) {
        self.isUserInteractionEnabled = enable
        self.alpha = enable ? 1 : 0.5
    }
}
