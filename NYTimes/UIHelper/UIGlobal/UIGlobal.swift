//
//  UIGlobal.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import Foundation
import UIKit

class UIGlobal: NSObject {
    
    // MARK: - UILabel Setup
    static func setupLabel(label: UILabel?,
                           labelText: String?,
                           textColor: UIColor?,
                           textFont: UIFont?,
                           attributedText: NSMutableAttributedString?,
                           textAlignment: Int?,
                           numberOfLines: Int?) {
        if attributedText != nil {
            label?.attributedText = attributedText
        } else {
            label?.text = labelText
            label?.font = textFont
            label?.textColor = textColor
        }
        
        label?.textAlignment = NSTextAlignment(rawValue: textAlignment!)!
        label?.numberOfLines = numberOfLines!
        label?.lineBreakMode = .byWordWrapping
    }
    
    //MARK:- UIRefreshControl
    static func addPullDownToRefreshToView(vc:UIViewController, view:UIView, action:Selector) {
        
        let refreshControl = UIRefreshControl()
        refreshControl.tag = GlobalTags.PULL_TO_REFRESH_TAG
        
        //Pull Down to refresh
        refreshControl.addTarget(vc, action: action, for: UIControl.Event.valueChanged)
        view.addSubview(refreshControl)
    }
    
    static func endRefreshingOnView(view:UIView, isTable:Bool = false) {
        let refreshControl = view.viewWithTag(GlobalTags.PULL_TO_REFRESH_TAG) as? UIRefreshControl ?? UIRefreshControl()
        refreshControl.endRefreshing()
        
    }
    
    static func startRefreshingOnView(view:UIView, isTable:Bool = false) {
        if isTable {
            let table = view as? UITableView ?? UITableView()
            table.contentOffset = CGPoint.init(x: 0, y: -60)
        }
        let refreshControl = view.viewWithTag(GlobalTags.PULL_TO_REFRESH_TAG) as? UIRefreshControl ?? UIRefreshControl()
        refreshControl.beginRefreshing()
    }
    
    // MARK: - Global Load More Cell
    static func loadMoreCell(loaderStyle:UIActivityIndicatorView.Style, color: UIColor) -> UIView {
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: loaderStyle)
        loadingIndicator.startAnimating()
        
        let loadingView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: (appDelegate?.window?.frame.size.width)!, height: 44)
        loadingView.backgroundColor = UIColor.clear
        loadingIndicator.center.x = (loadingView.center.x)
        loadingIndicator.center.y = (loadingView.center.y)
        loadingIndicator.color = color
        loadingView.addSubview(loadingIndicator)
        
        return loadingView
    }
    
    //MARK:- UIAlertAction
    static func showAlerController(title:String, message:String, dismissTitle:String, completion:@escaping (_ clicked:Bool) -> Void ) {
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction.init(title: dismissTitle, style: .default) { (clicked) in
            completion(true)
        }
        alertController.addAction(action)
        
        let topView = UIApplication.topViewController()
        topView?.present(alertController, animated: true, completion: {})
    }
    
}

