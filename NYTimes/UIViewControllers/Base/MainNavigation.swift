//
//  MainNavigation.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit

class MainNavigation: UINavigationController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Customize the main navigtion controller in the app.
        self.navigationBar.barTintColor = .black
        self.navigationBar.tintColor = UIColor.orange
        self.navigationBar.isTranslucent = false
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.orange]
        self.navigationBar.titleTextAttributes = textAttributes

    }
}
