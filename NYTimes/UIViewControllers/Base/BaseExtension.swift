//
//  BaseExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit

extension BaseViewController {
    
    //MARK:- Redirect To View Controller
    func redirectTo(storyboard: UIStoryboard, viewIdentifier: String, type: SegueType, animated: Bool = true) {
        let destinationViewController = storyboard.instantiateViewController(withIdentifier: viewIdentifier)
        switch type {
        case .push:
            self.navigationController?.pushViewController(destinationViewController, animated: true)
            break
        case .present:
            self.present(destinationViewController, animated: animated, completion: nil)
            break
        }
    }
    
    //MARK:- Dismiss Keyboard
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
}
