//
//  NewsView.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher
import SwiftDate

class NewsView: BaseViewController {
    
    //CoreData Stuff
    //Fetch Rooms from CoreData
    lazy var fetchedResultsController: NSFetchedResultsController? = { () -> NSFetchedResultsController<NewsResults>? in
        guard let moc = CoreDataStore.managedObjectContext else { return nil }
        let fetchRequest: NSFetchRequest<NewsResults> = NewsResults.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "publishedDate", ascending: false)]
        let _fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        _fetchedResultsController.delegate = self
        return _fetchedResultsController
    }()
    
    @IBOutlet weak var tableView:UITableView? {
        didSet {
            self.tableView?.register(UINib(nibName: CellIdentifier.NewsTableCell, bundle: nil), forCellReuseIdentifier: CellIdentifier.NewsTableCell)
            self.tableView?.tableFooterView = UIView()
            self.tableView?.estimatedRowHeight = 140
            self.tableView?.rowHeight = UITableView.automaticDimension
            self.tableView?.delegate = self
            self.tableView?.dataSource = self
            self.tableView?.backgroundColor = .clear
        }
    }
    
    //TableView Data Elements
    var fromRefresh = Bool()
    
    //Load more
    var currentPage = 1
    var shouldLoadMore = Bool()
    var fromLoadMore = Bool()
    
    //MARK:- View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK:- View Appear / View Disappear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Set my view title
        self.navigationItem.title = "NY Times"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK:- Setup View
    func setupView() {
        
        //Set Table cells seperator color
        self.tableView?.separatorColor = UIColor.orange.withAlphaComponent(0.5)
        
        //Pull down to refreh
        UIGlobal.addPullDownToRefreshToView(vc: self, view: tableView ?? UITableView(), action: #selector(refreshData))
        
        //Check if its fresh installation
        if UserDefaults.standard.bool(forKey: UserDefaultsKey.didFetchBefore) == false {
            UserDefaults.standard.set(true, forKey: UserDefaultsKey.didFetchBefore)
            UserDefaults.standard.synchronize()
            
            //Get news data & perform fetch after the completion.
            self.getNewsData(performFetch: true)
        }
        else {
            
            //Fetch Cached data.
            fetchCachedData()
            
            //Get Data - To update my core data with latest news...
            self.getNewsData()
        }
    }
    
    //MARK:- Fetch Cached Data
    func fetchCachedData() {
        
        try? self.fetchedResultsController?.performFetch()
        self.tableView?.reloadData()
    }
    
    //MARK:- ----------------------------- GET DATA -----------------------------
    @objc func refreshData() {
        
        fromRefresh = true
        resetLoadMore()
        self.getNewsData()
    }
    
    func resetLoadMore() {
        fromLoadMore = false
        shouldLoadMore = false
        currentPage = 1
    }

    //MARK:- Load More Method
    func loadMore() {
        if shouldLoadMore {
            fromLoadMore = true
            currentPage += 1
            tableView?.tableFooterView =  UIGlobal.loadMoreCell(loaderStyle: .white, color: .lightGray)
            self.getNewsData()
        }
        else {
            tableView?.tableFooterView = UIView()
        }
    }
    
    //MARK:- Device Optimize
    func deviceOptimize() {
        
    }
}

//MARK:- UITableViewDataSource
extension NewsView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchedResultsController?.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getNewsTableCell(indexPath: indexPath)
    }
}

//MARK:- UITableViewDelegate
extension NewsView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (self.fetchedResultsController?.fetchedObjects?.count ?? 0 ) > 0 {
            
            guard let item = self.fetchedResultsController?.fetchedObjects?[indexPath.row] else {return}
            
            let child = coreapp.mainStoryboard.instantiateViewController(withIdentifier: ViewIdentifier.NewsDetails) as? NewsDetails ?? NewsDetails()
            child.newsObject = item
            self.navigationController?.pushViewController(child, animated: true)
        
        }
    }
}

extension NewsView:NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch(type) {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            tableView?.insertRows(at: [newIndexPath], with: .none)
            break
        case .update:
            guard let indexPath = indexPath else { return }
            tableView?.reloadRows(at: [indexPath], with: .fade)
            break
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView?.deleteRows(at: [indexPath], with: .none)
            break
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            tableView?.deleteRows(at: [indexPath], with: .none)
            tableView?.insertRows(at: [newIndexPath], with: .none)
            break
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView?.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView?.endUpdates()
    }
}

