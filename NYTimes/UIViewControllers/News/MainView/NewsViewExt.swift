//
//  NewsViewExt.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import Foundation
import UIKit


extension NewsView {
    
    //MARK:- ----------------- GET NEWS DATA -----------------
    func getNewsData(performFetch:Bool = false) {
        
        if fromLoadMore == false && fromRefresh == false {
            UIGlobal.startRefreshingOnView(view: self.tableView ?? UITableView(), isTable: true)
        }
       
        //Perform API call
        api.getNewsListForSection(section: .all, period: .thirtyDays) { (data) in
            
            UIGlobal.endRefreshingOnView(view: self.tableView ?? UITableView())
            
            if data.status == ResponseStatus.success.rawValue {
                
                //Parse.
                Utilities.parseCodable(data: data.data, type: NewsModal.NewsMainObject.self, successHandler: { (final) in
                    
                    //Save response in core data
                    guard let moc = CoreDataStore.managedObjectContext else { return }
                    let _ = NewsResults.createOrFetch(models: final.results)
                    try? moc.save()
                    
                }, errorHandler: { (error) in
                    UIGlobal.showAlerController(title: error, message: "", dismissTitle: translate(key: "Ok"), completion: { (clicked) in})
                })
            }
            else {
                UIGlobal.showAlerController(title: data.message, message: "", dismissTitle: translate(key: "Ok"), completion: { (clicked) in})
            }
            
            if performFetch {
                self.fetchCachedData()
            }
            else {
                self.tableView?.reloadData()
            }
        }
    }
    
    //MARK:- ------------- TABLE CELLS HANDLERS --------------
    func getNewsTableCell(indexPath: IndexPath) -> UITableViewCell {
        guard let item = self.fetchedResultsController?.fetchedObjects?[indexPath.row] else {return UITableViewCell()}
    
        //Declare my cell
        let cell:NewsTableCell = tableView?.dequeueReusableCell(withIdentifier: CellIdentifier.NewsTableCell) as? NewsTableCell ?? NewsTableCell()
        
        //Title
        cell.titleLabel?.text = item.title ?? ""
        
        //By line & Date
        let byLine = item.byline ?? ""
        let date = (item.publishedDate ?? "").toDate()?.date ?? Date()
        let dateStr = date.toFormat(DateFormat.global_ui_dateformat_1)
        let final = Utilities.buildAttrStringFrom2Strings(string1Text: byLine + " " + translate(key: "on") + " ",
                                                          string1Color: .lightGray,
                                                          string1Font: UIFont.systemFont(ofSize: 15),
                                                          string2Text: dateStr,
                                                          string2Color: .lightGray,
                                                          string2Font: UIFont.systemFont(ofSize: 15))
        cell.byAndDateLabel?.attributedText = final
        
        //Image View
        let mediaMetadata = NewsMediaMetadatum.fetchMetadataForNewsId(newsId: item.id ) ?? [NewsMediaMetadatum]()
        var imageUrlStr = ""
        for item in mediaMetadata {
            let format = item.format ?? ""
            if format == NewsModal.Format.standardThumbnail.rawValue {
                imageUrlStr = item.url ?? ""
                break
            }
        }
        let imageUrl = URL.init(string: imageUrlStr)
        cell.imageV?.kf.setImage(with: imageUrl, placeholder: UIImage.init(named: "default_image"), options: nil, progressBlock: nil, completionHandler: { (finalImage, error, cacheType, imgUrl) in
            if error != nil {
                cell.imageV?.image = UIImage.init(named: "default_image")
            }
        })
    
        return cell
    }
}
