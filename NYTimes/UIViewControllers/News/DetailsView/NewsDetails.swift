//
//  NewsDetails.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftDate

class NewsDetails: BaseViewController {
    
    //Outlets
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var titleAndDateLabel:UILabel! {
        didSet {
            UIGlobal.setupLabel(label: titleAndDateLabel,
                                labelText: "",
                                textColor: .white,
                                textFont: UIFont.systemFont(ofSize: 23),
                                attributedText: nil,
                                textAlignment: NSTextAlignment.left.rawValue,
                                numberOfLines: 0)
        }
    }
    @IBOutlet weak var descLabel:UILabel!{
        didSet {
            descLabel.numberOfLines = 0
        }
    }
    
    //Passed var
    var newsObject:NewsResults?
    
    //MARK:- View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK:- View Appear / View Disappear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK:- Setup View
    func setupView() {
    
        //Set the image view
        let mediaMetadata = NewsMediaMetadatum.fetchMetadataForNewsId(newsId: self.newsObject?.id ?? 0) ?? [NewsMediaMetadatum]()
        var imageUrlStr = ""
        for item in mediaMetadata {
            let format = item.format ?? ""
            if format == NewsModal.Format.large.rawValue || format == NewsModal.Format.jumbo.rawValue  || format == NewsModal.Format.superJumbo.rawValue {
                imageUrlStr = item.url ?? ""
                break
            }
        }
        let imageUrl = URL.init(string: imageUrlStr)
        self.imageView?.kf.setImage(with: imageUrl, placeholder: UIImage.init(named: "default_image"), options: nil, progressBlock: nil, completionHandler: { (finalImage, error, cacheType, imgUrl) in
            if error != nil {
                self.imageView?.image = UIImage.init(named: "default_image")
            }
        })
        
        //Setup title label
        titleAndDateLabel.text = self.newsObject?.title ?? ""
        
        //Setup description & date
        let abstract = self.newsObject?.abstract ?? ""
        let date = (self.newsObject?.publishedDate ?? "").toDate()?.date ?? Date()
        let dateStr = date.toFormat(DateFormat.global_ui_dateformat_1)
        let final = Utilities.buildAttrStringFrom2Strings(string1Text: abstract + "\n\n",
                                                          string1Color: .lightGray,
                                                          string1Font: UIFont.systemFont(ofSize: 21),
                                                          string2Text: dateStr,
                                                          string2Color: .lightGray,
                                                          string2Font: UIFont.systemFont(ofSize: 18))
        descLabel.attributedText = final
        
        //Device Optimize
        deviceOptimize()
    }
    
    
    //MARK:- Device Optimize
    func deviceOptimize() {
        
    }
    
}
