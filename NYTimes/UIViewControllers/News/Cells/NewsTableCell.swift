//
//  NewsTableCell.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import UIKit

class NewsTableCell: UITableViewCell {

    @IBOutlet weak var imageV:UIImageView?  {
        didSet {
            imageV?.layer.cornerRadius = 8
            imageV?.contentMode = .scaleAspectFill
            imageV?.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var titleLabel:UILabel? {
        didSet {
            UIGlobal.setupLabel(label: titleLabel,
                                labelText: "",
                                textColor: .white,
                                textFont: UIFont.systemFont(ofSize: 18),
                                attributedText: nil,
                                textAlignment: NSTextAlignment.left.rawValue,
                                numberOfLines: 0)
        }
    }
    
    @IBOutlet weak var byAndDateLabel:UILabel? {
        didSet {
            byAndDateLabel?.numberOfLines = 0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }    
}
