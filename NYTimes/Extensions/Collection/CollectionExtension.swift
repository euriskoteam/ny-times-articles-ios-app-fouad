//
//  CollectionExtension.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import Foundation

extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
