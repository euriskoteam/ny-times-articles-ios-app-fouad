
## NYTimes iOS App

# This app is using the below pods as dependencies
  - ReachabilitySwift: For networking changing detection
  - Fabric & Crashlytics: For crash reproting
  - SwiftDate: Easy & pure Swift library to handle all dates conversion 

# Main features
  - Black themed app listing the mostpopular & mostviewed news.
  - Easy to change news data source by simple enum switch.
  - Offline data caching so you can read the news anytime you want.
  
You can also:
  - Change the news source by going to 'NewsViewExt.Swift'
  - Find "api.getNewsListForSection(section: .all, period: .thirtyDays)"
  - Change the 'section:' and/or 'period:' enums to get data as your needs
  
# Automation & UniTests
- This app is using fastlane command lines for CD/CI
- A Jenkins server is connected to this project's remote repo.
- The Jenkins server will trigger a build once the source code is pushed to 'master'.
- The sever will prepare the app for release with app metadata within the 'fastlane' folder.

# Fastlane
The below commands are used & deployed on Jenkins, more details in 'NYTimes Config [Jenkins].html'

```sh
$ fastlane scan #To build the UnitTests & UITests 
$ fastlane fastlane-credentials ## To Add the ApplID that used with Apple Developer & iTunes Connect
$ fastlane produce #To Create the app on Apple Developer Portal if it wasn't created before
$ fastlane match development #To Create the profiles and certs on Apple Developer Portal & sync them with git
$ fastlane match appstore #Same as above
$ fastlane gym #To Build & Sign the app and export it as .ipa file
```
